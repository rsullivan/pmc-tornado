import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.httpclient
import tornado.gen

import urllib
import json
import datetime
import time

"""
Example taken from Introduction to Tornado
by Brendan Berg, Adam Parrish, Michael Dory
O'Reilly Media, Inc.
March 2012
"""

from tornado.options import define, options
define("port", default=8000, help="run on given port", type=int)

class IndexHandler(tornado.web.RequestHandler):
	@tornado.web.asynchronous
	@tornado.gen.engine
	def get(self):
		query = self.get_argument('doi')
		client = tornado.httpclient.AsyncHTTPClient()

		response = yield tornado.gen.Task(client.fetch,
			"https://pmc-ref.herokuapp.com/api/v1/references/free/?"+ \
			urllib.parse.urlencode({"doi": query}))
		
		body = json.loads(response.body.decode())
		self.write(body)
		self.finish()

		


if __name__ == "__main__":
	tornado.options.parse_command_line()
	app = tornado.web.Application(handlers=[(r"/", IndexHandler)])
	http_server = tornado.httpserver.HTTPServer(app)
	http_server.listen(options.port)
	tornado.ioloop.IOLoop.instance().start()